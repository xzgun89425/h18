module.exports = {
    apps : [{
        name:'H18',
        script: './node_modules/nuxt-start/bin/nuxt-start.js',
        instances: 'max',
        exec_mode : "cluster",
        max_memory_retart:' 1G',
        port: 3000
    }]
};